#!/usr/bin/env python3


import cv2
import pandas as pd
import numpy as np
import os
import sys
from sklearn.preprocessing import normalize
from sklearn.cluster import KMeans

if __name__ == "__main__":
    path = os.popen('pwd').read().strip('\n')
    print (path)
    features = pd.read_csv(
    filepath_or_buffer='{path}/x_train_gr_smpl.csv'.format(path=path),
    sep=',', na_values='None', decimal='.').values

    features_and_classes = pd.read_csv(
    filepath_or_buffer='{path}/final.csv'.format(path=path),
    sep=',', na_values='None', decimal='.').values

    classes = pd.read_csv(
    filepath_or_buffer='{path}/y_train_smpl.csv'.format(path=path),
    sep=',', na_values='None', decimal='.').values

    msk = np.random.rand(len(features)) < 0.8
    x_train = features[msk]
    #x_train = x_train[:len(x_train)//2]
    
    y_train = np.transpose(classes[msk])[0]    
    x_train = x_train.astype('float') / 255
    x_test = x_test.astype('float') / 255

    #Clustering algorithm
    k_means = KMeans(n_clusters=10)
    k_means.fit(x_train)
    j = 0
    k = 0
    for i in range(len(x_train)):
        if k_means.predict(x_train[i].reshape(1, -1))[0] == y_train[i]:
            j += 1
        else:
            k += 1
    print(j) # Number of Success
    print(k) # Number of Failures