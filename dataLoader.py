import pandas as pd
import sklearn as sk
import numpy as np


features = pd.read_csv(
    filepath_or_buffer='{path}/x_train_gr_smpl.csv'.format(path='/home/msc/ao79/dataminingcw1'),
    sep=',', na_values='None', decimal='.')

classes = pd.read_csv(
    filepath_or_buffer='{path}/y_train_smpl.csv'.format(path='/home/msc/ao79/dataminingcw1'),
    sep=',', na_values='None', decimal='.')

classes.index.
roadsigns = pd.concat([features, classes], axis = 1)
roadsigns = sk.utils.shuffle(roadsigns)
