#!/usr/bin/env python3

import cv2
import pandas as pd
import numpy as np
import os
import sys
from keras.datasets import mnist
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.model_selection import train_test_split
from sklearn.manifold import TSNE
from sklearn.preprocessing import normalize
from sklearn.cluster import KMeans

if __name__ == "__main__":
    path = os.popen('pwd').read().strip('\n')
    print (path)
    features = pd.read_csv(
    filepath_or_buffer='{path}/x_train_gr_smpl.csv'.format(path=path),
    sep=',', na_values='None', decimal='.').values


    classes = pd.read_csv(
    filepath_or_buffer='{path}/y_train_smpl.csv'.format(path=path),
    sep=',', na_values='None', decimal='.').values
    
    msk = np.random.rand(len(features)) < 0.8
    x_train = features[msk]
    x_test = features[~msk]

    y_train = np.transpose(classes[msk])[0]
    y_test = np.transpose(classes[~msk])[0]
    
    x_train = x_train.astype('float') / 255
    x_test = x_test.astype('float') / 255

    
    x_train = x_train.reshape(-1, 48*48)
    x_test = x_test.reshape(-1, 48*48)

    # (z_train, u_train), (z_test, u_test) = mnist.load_data()
    # z_train = z_train.astype('float') / 255
    # z_test = z_test.astype('float') / 255
    # z_train = z_train.reshape((-1, 28*28))

    np.apply_along_axis(
        func1d=lambda img: cv2.resize(img.reshape((48, 48)), (32, 32)) ,axis=1, arr=x_train
    ).reshape(-1, 32 * 32)

    x_train_sm, _, y_train_sm, _ = train_test_split(x_train, y_train, test_size=.9)
    #z_train_sm, _, u_train_sm, _ = train_test_split(z_train, u_train, test_size=.95)
    
    x_train_emb3 = TSNE(n_components=3, perplexity=50, n_iter=5000).fit_transform(x_train_sm)
    #z_train_emb3 = TSNE(n_components=3, perplexity=35).fit_transform(z_train_sm)
    fig = plt.figure()
    ax = Axes3D(fig)
    for label in range(10):
        x_train_tmp = x_train_emb3[y_train_sm==label]
        ax.scatter(x_train_tmp[:,0], x_train_tmp[:,1], x_train_tmp[:,2], alpha=0.75, label=label)
    ax.legend()
    plt.show()