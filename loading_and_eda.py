# -*- coding: utf-8 -*-
"""
Created on Mon Aug 26 22:41:13 2019
loading_and_eda.py
@author: Daniel Kienitz
"""

import cv2  # OpenCV
import matplotlib.pyplot as plt
import numpy as np
plt.style.use('ggplot') # Just for the plot style, there are many more
import pandas as pd
import re
from scipy import stats
import seaborn as sns
from sklearn.tree import DecisionTreeClassifier, plot_tree
import statsmodels.api as sm


# TODO: Change the file path to the directory where the csv files 
# and image are located:
path = 'D:/Universitaet/F21DL'


#### +++ Load csv file +++ ####

# index_col = 0: use first column as row names
# na_values: set values for 'not a number (NaN)' placeholder for missing values
mtcars = pd.read_csv(
    filepath_or_buffer='{path}/mtcars.csv'.format(path=path), 
    sep=';', index_col=0, na_values='None', decimal=',')

print('(number of rows, number of columns): ', mtcars.shape)
print('Column names: ', mtcars.columns.tolist())

# n: how many rows to print
print('-> First n rows: ', mtcars.head(n=5))
print('-> Last n rows: ', mtcars.tail(n=5))

print('Row names: ', mtcars.index.tolist())

# 'object' is Pnadas equivalent of Python's 'str':
print('Column types: ', mtcars.dtypes)

# Check for missing values, remember to set the missing value
# placeholder in 'pd.read_csv()':
print('Any NAs: ', mtcars.isnull().values.any())


#### +++ Adding and deleting columns +++

# Add a new column:
def mpg_to_lp100km(mpg_val):
    """
    Convert miles per gallon value in liters per 100 km.
    
    :param mpg_val, float/int; miles per gallon
    
    :return lp100km_val, float/int; liters per 100 km
    """
    
    lp100km_val = 100 * 3.785411784 / (1.609344 * mpg_val)
    
    return lp100km_val

# Apply function 'mpg_to_lp100km' to every element in the column 'mpg',
# and store the result in a new column 'lp100km_2':
mtcars['lp100km_2'] = mtcars['mpg'].apply(func=mpg_to_lp100km)

print('Dataframe: ', mtcars.head(n=5))
print('Datframe dimension: ', mtcars.shape)
print('lp100km_2: ', mtcars['lp100km_2'])

# More concise with lambda expressions, same function as 'mpg_to_lp100km':
mtcars['lp100km'] = mtcars['mpg'].apply(
    lambda mpg_val: 100 * 3.785411784 / (1.609344 * mpg_val))

# Side note: lambda funtions:
# THIS IS BAD PRACTICE! Strength of lambda functions is to not be
# needed to be assigned to a varible:
times_100 = lambda value: value*100

print('Lambda function: 2.5*100 = ', times_100(2.5))

# Test whether they are the same columns:
print(
  ' -> Do the columns "lp100km" and "lp100km_2" contain the same values per row?',
  all(mtcars['lp100km'] == mtcars['lp100km_2']))

# Get all column names with 'lp100km' in them (this is called a list 
# comprehension = more readable for-loop):
remove_bools = [
    bool(re.search(pattern='lp100km', string=col_name)) for col_name in mtcars.columns]

# Shorter but more pandas-like:
remove_bools_pd = mtcars.columns.str.contains('lp100km').tolist()

print('-> The same bools?', remove_bools == remove_bools_pd)

print('Column names (before): ', mtcars.columns, len(mtcars.columns))

# Delete columns by name, this is not in-place, 
# but several functions have a parameter called 'in-place',
# axis=1: drop colums
mtcars = mtcars.drop(labels=mtcars.columns[remove_bools].tolist(), axis=1)

print('Column names (after): ', mtcars.columns, len(mtcars.columns))

# Row names as new column 'name', loc is the position, so 0 means as the 
# very first column:
mtcars.insert(loc=0, column='name', value=mtcars.index.values.tolist())

print('-> Dataframe head: ', mtcars.head(n=5))

print('Data types: ', mtcars.dtypes)

# Reset row index; drop=True remove old row names without adding 
# them as a new column; can also be used to directly set the row names 
# as a new column when drop=False:
mtcars = mtcars.reset_index(drop=True)

print('-> Dataframe head: \n', mtcars.head(n=5))

# Rename cell values from Merc to Mercedes, can be used with regex:
mtcars = mtcars.replace(to_replace='Merc', value='Mercedes', regex=True)


### +++ Selecting +++ ####

# Dataframe with only the Merecedes, then reset index otherwise row indices
# are the ones form the original data frame:
mercs = mtcars[mtcars['name'].str.contains('Mercedes')].reset_index(drop=True)

print('All the mercedes: \n', mercs)
print('Number of mercedes: ', mercs.shape)

# Sort the cars by 'cyl' and then within each 'cyl'-group by 'hp':
mercs = mercs.sort_values(
    by=['cyl', 'hp'], axis=0, ascending=False)

print('All Mercedes: ', mercs)

# Boolean indexing: Dataframe with only the most powerful mercedes:
fastest_mercs = mtcars[
    (mtcars['name'].str.contains('Mercedes')) & (mtcars['hp']>100)]

print('Fastest Mercedes: ', fastest_mercs)

# Sort the eight cylinder cars by horsepower:
eight_cyl = mtcars[mtcars['cyl']>=8].sort_values(
    by='hp', ascending=False).reset_index(drop=True)

print('Eight cylinder Mercedes: ', eight_cyl)


### +++ OpenCV +++ ####

# Very short intro, just to make you aware of it:
# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html

# If this does not work, image is 'None':
# OpenCV uses BGR as default color scheme, so the car is blue instead of red:
image = cv2.imread('{path}/maserati_bora.jpg'.format(path=path))

# (1044, 1567, 3) = (height, width, BGR-color):
# https://www.w3schools.com/colors/colors_rgb.asp
print('Resolution: ', image.shape)

plt.figure()
plt.imshow(image)
plt.title('BGR color scheme')
plt.suptitle('Car is blue, so are the stones')
plt.show()

# Convert between color model, here BGR to RGB:
image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

plt.figure()
plt.imshow(image_rgb)
plt.title('RGB color scheme')
plt.suptitle('Now the car is red and so are the stones')
plt.show()

# To grey scale, for an MLP for example:
image_grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# RGB-channel is gone, now each entry in the matrix denotes the pixel 
# intensity ranging from o=white to 255=black.
print('Shape in grey scale: ', image_grey.shape)

plt.figure()
plt.imshow(image_grey,  cmap='Greys')
plt.title('Grey color scheme')
plt.show()

# Rescaling image:
# (width, height)
image_grey_rs = cv2.resize(image_grey, (48, 48))

print('Nre image shape: ', image_grey_rs.shape)

plt.figure()
plt.imshow(image_grey_rs, cmap='Greys')
plt.title('Decreased resulution')
plt.show()

# Second image:
image_2 = cv2.cvtColor(
    cv2.imread('{path}/s_klasse.jpg'.format(path=path)),
    cv2.COLOR_BGR2RGB)
                       
plt.figure()
plt.imshow(image_2)
plt.title('Die S-Klasse.')
plt.show()

image_2 = cv2.resize(
    cv2.cvtColor(image_2, cv2.COLOR_RGB2GRAY), (48, 48))
                       
plt.figure()
plt.imshow(image_2, cmap='Greys')
plt.title('Decresed resolution')
plt.show()

# Transform both images from square matrices to row vectors:
image_1 = image_grey_rs.reshape((1, -1))
image_2 = image_2.reshape((1, -1))

images = [image_1, image_2]

# Stack them row-wise:
image_arr = np.vstack(images)

# The images represented as row vectors;
# same representation as in the CW: images are grey-scaled row vectors
print('Image representation: ', image_arr.shape)

plt.figure()
plt.imshow(image_arr[0].reshape((48, 48)))
plt.title('First image')
plt.show()


### +++ Explorative data analysis +++ ####

# min, max, standard de. of all numericl columns:
print('Statistics of numerical columns: ', mtcars.describe())
print('Statistics of mpg-column: ', mtcars['mpg'].describe())
print('Statistics of name-column: ', mtcars['name'].describe())

# Default: Pearson:
corr_mat = mtcars.corr()
print('Correlation matrix between numerical columns: ', corr_mat) 


#### +++ Visualizations +++ ####

# seaborn is build on top of matplotlib.

# For kernel-density estimators, mind the bin number 'bins':
plt.figure()
sns.distplot(a=mtcars['mpg'], bins=10)
plt.title('Distribution of mpg values')
plt.ylabel('Relative frequency')
plt.xlabel('Miles per gallon')
plt.show()

# Joint and single distributions:
sns.jointplot(x=mtcars['hp'], y=mtcars['mpg'], kind='kde')

# Add regression stats, the author get a depreciation warning
# though ...
# p-value is the p-value of the slope; square of Pearons-R is R^2 metric
plot_ = sns.jointplot(x=mtcars['hp'], y=mtcars['mpg'])
plot_.annotate(stats.pearsonr)
plt.show()

# Use violin plots instead of boxplots:
plt.figure()
sns.violinplot(x=mtcars['cyl'], y=mtcars['mpg'])
plt.title('Distribution of mpg value over cylinder count')
plt.show()

plt.figure()
sns.violinplot(x=mtcars['cyl'], y=mtcars['mpg'], hue=mtcars['vs'], split=True)
plt.title('Distribution of mpg values over cylinder count by cylinder shape')
plt.show()

# 'vs' is of type integer, we need 'string'
print("Type of 'vs' column: ", mtcars['vs'].dtype)

# Replace values in 'vs' with strings:
mtcars['vs'] = mtcars['vs'].apply(str)

print("Type of 'cyl' column: ", mtcars['vs'].dtype)

# Replace string with new strings:
mtcars['vs'] = mtcars['vs'].map({'0':'v-shaped', '1':'straight'})

print("'vs' column: ", mtcars['vs'])

plt.figure()
sns.violinplot(x=mtcars['cyl'], y=mtcars['mpg'], hue=mtcars['vs'], split=True)
plt.title('Distribution of mpg values over cylinder count by cylinder shape')
plt.show()

# The correlation coefficients visualized with colors:
plt.figure()
sns.heatmap(corr_mat, annot=True, cmap='coolwarm')
plt.title('Colored Pearson correlation coefficients')
plt.show()


#### +++ Regression +++ ####

# Pandas fits nicely with other Python packages as well. For
# example with 'statsmodels'.
# Statsmodels offers lots of regression models, really worth
# to check out:
# https://www.statsmodels.org/stable/index.html

# Ordinary-least squares regression: mpg = a*hp + b,
# a = intercept, b = slope
reg_model = sm.OLS(endog=mtcars['hp'], exog=mtcars['mpg']).fit()
# R^2 = pearsons-r squared:
print(reg_model.summary())


#### +++ Create a pipeline +++ ####

# Load timeseries with currency:
# Lots of other economic timeseries and data free to download:
# https://fred.stlouisfed.org/
usd_gbp = pd.read_csv('{path}/DEXUSUK.csv'.format(
    path=path), decimal='.', na_values='.')

print('Exchange rates: ', usd_gbp)

# On hholidays and weekends Forex is closed:
print('Any NAs: ', usd_gbp.isnull().values.any())

# Remove missing values:
usd_gbp = usd_gbp.dropna()

print('Any NAs: ', usd_gbp.isnull().values.any())

print('Length: ', usd_gbp.shape)
print('Data types: ', usd_gbp.dtypes)

# 'DATE'-column form string to Pandas-datetime:
usd_gbp['DATE'] = pd.to_datetime(usd_gbp['DATE'])

print('Data types: ', usd_gbp.dtypes)

plt.figure()
plt.plot(usd_gbp['DATE'], usd_gbp['DEXUSUK'])
plt.title('Exchange rate: US-Dollar, GB-Pound')
plt.show()

# If certain operations need to be done for multiple data sets,
# pipelines offer a convenient and easy to read way to apply them.
# Similar to R's magrittr pipes ...

# Define some functions for the loaded dataframe:
def append_date(df):
    """
    Splits up the 'DATE' column of the original dataframe into three
    column with with year, month and day.
    
    :param df, pandas dataframe
    
    :return _, pandas dataframe
    """
    
    # From datetime to string:
    df['DATE'] = df['DATE'].astype(str)
    # Alternatively:
#    df['DATE'] = df['DATE'].apply(lambda date: date.strftime('%Y-%m-%d'))
    
    date_df = pd.DataFrame(
        df['DATE'].str.split('-').tolist(), 
        columns=['YEAR', 'MONTH', 'DAY'])

    return pd.concat(
        [df.reset_index(drop=True), 
         date_df.reset_index(drop=True)], axis=1)


def add_month_names(df, colname):
    """
    Replaces the month number with names.
    
    :param df, pandas dataframe
    :param colname, string; name of the column containg the month

    :return _, pandas dataframe
    """
    
    # 'key': old value; 'value': new value
    df[colname] = df[colname].map(
        {'01':'Jan.', '02':'Feb.', '03':'Mar.', '04':'Apr.', '05':'May',
         '06':'Jun.', '07':'July', '08':'Aug.', '09':'Sep.', '10':'Oct.',
         '11':'Nov.', '12':'Dec.'})
    
    return df


# df.pipe(func) apply the function 'func' on dataframe 'df',
# if muliple pipe are stacked, the resulting dataframe is 
# passed to the next pipe-operator on the right.
# Works also with functions that receives multiple parameters and
# also with lambda operators.
usd_gbp_new = usd_gbp.pipe(append_date).pipe(add_month_names, colname='MONTH').pipe(
    lambda df: df.drop(labels='DATE', axis=1))

print('Head:\n', usd_gbp_new.head())


#### +++ Creating dataframes in a for loop +++ ####

rows = []

# Each iteration one row of data is created:
for draw_num in range(10):

    random_var = np.random.normal(loc=0.0, scale=1.0, size=1)[0]
    
    # Keys are column names, values are column values:
    draw = {'draw_num':draw_num, 'rv':random_var}
    
    rows.append(draw)

rvs_df = pd.DataFrame(rows)

print('rvs_df:\n', rvs_df)


#### +++ Decision tree +++ ####

# An sklearn decision tree classifier:
dec_tree = DecisionTreeClassifier()

# Predict whether Mercedes or not:
labels = np.zeros(shape=(mtcars.shape[0], 1))

# If entry is a Mercedes 1 else 0:
labels[mtcars['name'].str.contains('Mercedes').tolist()] = 1

print('mtcars labels: ', labels)

# Only select columns with numeric data:
mtcars_num = mtcars.select_dtypes(include=['float64', 'int64'])

print('Numeric data frame: ', mtcars_num.shape)

dec_tree = dec_tree.fit(X=mtcars_num, y=labels)

# Left is True, right is false:
plot_tree(
    decision_tree=dec_tree, 
    feature_names=mtcars_num.columns.tolist(),
    filled=True, class_names=['Not Mercedes', 'Mercedes'])

# Prediction for a certain car:
print('Prediction:', dec_tree.predict(X=np.array(mtcars_num.iloc[7]).reshape((1, -1))))

print('Accuracy: ', dec_tree.score(X=mtcars_num, y=labels))

print('Probabilites for all entries: ', dec_tree.predict_proba(X=mtcars_num))

# 2. row , left node: there is no Mercedes in the data frame,
# so the color of the leaf is dark-orange, meaning every car 
# is predicted to be 'not-Mercedes':
print(mtcars[(mtcars['qsec']<=17.35)])

# 3. row, left node: again, all prediction at this leaf 
# are not-Mercedes, which is correct:
print(mtcars[(mtcars['qsec']>17.35) & (mtcars['carb']<=1.5)])

# 3. row, right node: node is light-blue, so most instance 
# at this node are predicted to be Mercedes:
print(mtcars[(mtcars['qsec']>17.35) & (mtcars['carb']>1.5)])

# Going to the largest dark-blue leaf, there are all the Mercedes:
print(mtcars[
   (mtcars['qsec']>17.35) & (mtcars['carb']>1.5) & 
   (mtcars['hp']<=192.5) & (mtcars['wt']>2.965)])

# Note: For this data frame the decsion tree is 100% correct, 
# so all leafs are dark-colored.
    
    